import React from "react";
import "./_modal.scss";

export const Actions = ({ firstBtn, secondBtn }) => (
  <div className="modal_btns">
    <button className="modal_submit">{firstBtn}</button>
    <button className="modal_cancel">{secondBtn}</button>
  </div>
);
export const Modal = (props) => {
  return (
    <div
      className={`modal_wrapper ${props.closeBtn ? "open" : "close"}`}
      style={{ ...props.style }}
      onClick={(e) => e.currentTarget === e.target && props.onModalClose()}
    >
      <div className="modal_body">
        <div className="modal_header">
          <h2 className="modal_title">{props.header}</h2>
          <button className="modal_close" onClick={props.onModalClose}>
            X
          </button>
        </div>
        <p className="modal_text">{props.text}</p>
        {props.actions}
      </div>
    </div>
  );
};
