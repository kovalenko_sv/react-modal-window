import React from "react";

export const Button = (props) => {
  return (
    <button style={props.style} onClick={props.onClick}>
      {props.text}
    </button>
  );
};
