import { Modal, Actions } from "./components/Modal";
import { Button } from "./components/Button";
import { useState } from "react";

function App() {
  const [modal, setModal] = useState({
    modal1: false,
    modal2: false,
  });

  return (
    <div className="App">
      <Button
        style={{
          backgroundColor: "brown",
          color: "white",
          border: "none",
          borderRadius: "5px",
        }}
        className="first-btn"
        onClick={() =>
          setModal({
            ...modal,
            modal1: true,
          })
        }
        text="Open first modal"
      />
      <Button
        style={{
          backgroundColor: "orange",
          color: "white",
          border: "none",
          borderRadius: "5px",
        }}
        onClick={() =>
          setModal({
            ...modal,
            modal2: true,
          })
        }
        text="Open second modal"
      />

      <Modal
        onModalClose={() => setModal({ modal1: false })}
        closeBtn={modal.modal1}
        header="Do you want to delete this file?"
        text="Once you delete this file, it won't be possible to undo this action. 
        Are U sure to delete this file?"
        actions={<Actions firstBtn="OK" secondBtn="CANCEL" />}
      />
      <Modal
        onModalClose={() => setModal({ modal2: false })}
        closeBtn={modal.modal2}
        header="Hi there, I'm Modal v.2!"
        text="Once upon a time... 
        Are U sure to close me?"
        actions={<Actions firstBtn="Hmm.." secondBtn="YEP!" />}
      />
    </div>
  );
}

export default App;
